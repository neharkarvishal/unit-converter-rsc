import { cookies } from 'next/headers';
import { NextResponse } from 'next/server';

export async function GET(request: Request) {
  return NextResponse.json(
    {
      message: 'Hello, Next.js!',
    },
    {
      status: 200,
      statusText: 'Success',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': 'Content-Type, Authorization',
      },
    },
  );
}
export async function POST(request: Request) {
  let { fromUnit, toUnit, fromUnitVal, toUnitVal } = await request.json();

  let res = {
    fromUnit,
    toUnit,
    fromUnitVal,
    toUnitVal,
  };

  return NextResponse.json(res, {
    status: 200,
    statusText: 'Success',
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
      'Access-Control-Allow-Headers': 'Content-Type, Authorization',
    },
  });
}
