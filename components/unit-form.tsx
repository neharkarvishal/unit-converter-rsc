'use client';
import { useEffect, useState, useCallback, useMemo } from 'react';

export default function UnitForm({ payload }) {
  let [fromUnit, setFromUnit] = useState('km');
  let [fromUnitVal, setFromUnitVal] = useState<number | null>(null);

  let [toUnit, setToUnit] = useState('m');
  let [toUnitVal, setToUnitVal] = useState<number | null>(null);

  useEffect(() => {
    fetch('/api', {
      method: 'POST',
      body: JSON.stringify({ fromUnit, toUnit, fromUnitVal, toUnitVal }),
    })
      .then(res => {
        return res.json();
      })
      .then(res => {
        console.log('res', res);
      });
  }, [fromUnit, toUnit, fromUnitVal, toUnitVal]);

  return (
    <div>
      <form id="converter-form">
        <label htmlFor="input-value">From</label>
        <input
          type="number"
          id="input-value"
          required
          value={fromUnitVal}
          onChange={e => {
            setFromUnitVal(() => e.target.valueAsNumber);
          }}
        />

        <label htmlFor="from-unit">From Unit</label>
        <select
          id="from-unit"
          required
          value={fromUnit}
          onChange={e => {
            setFromUnit(e.target.value);
          }}
        >
          <option value="km">Kilometers (KM)</option>
          <option value="m">Meters (M)</option>
          <option value="cm">Centimeters (CM)</option>
        </select>

        <br />

        <label htmlFor="output-value">To</label>
        <input
          type="number"
          id="output-value"
          required
          value={toUnitVal}
          onChange={e => {
            setToUnitVal(() => e.target.valueAsNumber);
          }}
        />

        <label htmlFor="to-unit">To Unit</label>
        <select
          id="to-unit"
          required
          value={toUnit}
          onChange={e => {
            setToUnit(e.target.value);
          }}
        >
          <option value="km">Kilometers (KM)</option>
          <option value="m">Meters (M)</option>
          <option value="cm">Centimeters (CM)</option>
        </select>
      </form>
    </div>
  );
}
